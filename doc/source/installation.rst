Installation
============
BOSS is written in Python 3 and distributed as a PyPI package. 

Recommended
+++++++++++
We recommend installing BOSS inside a virtual enviroment (``venv``, ``conda``...). The latest stable version can be installed using pip:

.. code-block:: sh

    python3 -m pip install aalto-boss

User-installation
+++++++++++++++++++
If you are not using virtual enviroments, we recommend doing a user-installation with pip:

.. code-block:: sh

    python3 -m pip install --user aalto-boss

On Linux, this will place the python module under ``$HOME/.local/lib/python3.X/site-packages`` where X should be substituted for the minor version of your Python distribution. During the installation an executable called ``boss``, which is used to run BOSS, is built and placed under ``$HOME/.local/bin``. In order for your BOSS installation to function properly, these paths must (if not already present) be appended to the correct enviroment variables:

.. code-block:: sh

   export PATH=$PATH:$HOME/.local/bin
   # Do not forget to substitute X for your Python minor version 
   export PYTHONPATH=$PYTHONPATH:$HOME/.local/lib/python3.X/site-packages

To check that the installation was successful, calling the ``boss`` executable without any arguments should result in usage instruction being printed. During the installation you may see a large number of dependencies being installed by pip. Notably, BOSS uses the Gaussian process framework  `GPy <http://github.com/SheffieldML/GPy>`_ as a backend.

Development version
+++++++++++++++++++
To install the development version, clone the source code and install with pip:

.. code-block:: sh

    git clone https://gitlab.com/cest-group/boss.git
    cd boss
    python3 -m pip install --user .

Known issues
+++++++++++++++++++
* Rarely, on HPC systems, runtime errors related to the use of cython in GPy have been observed. This can typically be resolved by using a more fully-featured Python distribution that ships with cython, such as `Anaconda <https://www.anaconda.com/distribution/>`_.

* BOSS is frequently run on HPC queues in parallel jobs that accommodate data acqusitions.
  Please be sure to set the system variable ``export OMP_NUM_THREADS=1`` for optimal performance
  in serial jobs (for parallelisation in BOSS, check the :ref:`keywords <Keywords>`). 
  On some systems, it may be needed to set:

.. code-block:: sh

	export MKL_NUM_THREADS=1
	export NUMEXPR_NUM_THREADS=1
	export OMP_NUM_THREADS=1

