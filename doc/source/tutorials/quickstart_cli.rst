.. _quickstart_cli:

Quickstart
----------------

Running the BOSS executable requires an ascii **input file** (any name) and a customised **user function** Python script which facilitates data acquisition. The input file keywords and default values are listed :ref:`here<Keywords>`. Unless explicitly specified in the input file, variables will be assigned default values. 

An example minimal input file ``boss.in`` (filename is free to choose) is shown below:

.. code-block:: python

    # boss.in
    userfn        simple_function.py
    bounds        0 7
    yrange        -1 1
    kernel        rbf
    initpts       5
    iterpts       15

The input above will instruct BOSS to minimize a non-periodic function subject to the constraint *0 < x < 7*, where the expected range of y values is *-1 < y < 1*. Some initial points (*initpts*) are needed to start and the simulation will perform 15 Bayesian optimisation iterations (*iterpts*).

For any *x* queried by the BOSS acquisition function, the result will be returned by the ``simple_function.py``, for example:

.. code-block:: python

   import numpy as np

    def f(X):
        x = X[0, 0]
        return np.sin(x) + 1.5*np.exp(-(x - 4.3)**2)


The optimization can now be started from the command line:

.. code-block:: bash

    $ boss o boss.in

BOSS has two principal output files, called ``boss.out`` and ``boss.rst`` by default. A summary of the simulation and key findings can be found in ``boss.out``. Simulation restarts are facilitated by ``boss.rst``, which contains the history of acquisitions and fitted model parameters. Note that BOSS also has extensive capabilities for automatic post-processing, including visualization of data acquisition trends, simulation convergence and snapshots of the surrogate models (see :ref:`post-processing<Data post-processing>` and tutorials for details).
