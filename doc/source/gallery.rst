Gallery
===========

.. comment - image:: figures/BOSS_collage-768x477.png

.. figure:: figures/BOSS_collage-768x477.png
   :scale: 100 %
   :alt: BOSS examples

   Caption: BOSS-generated energy and property landscapes for various molecular conformers and surface adsorbates.

