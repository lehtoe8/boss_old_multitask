What is BOSS
================================

.. figure:: figures/boss_donut.png
   :scale: 70 %
   :alt: BOSS examples

.. comment   Caption: This is BOSS.

BOSS uses Bayesian optimization to build N-dimensional surrogate models for the energy or property landscapes and infer global minima. The models are iteratively refined by sequentially sampling DFT data points that are promising and/or have high information content. In computational structure search, representing heterogenous materials with compact chemical ‘building blocks’ allows us to build in prior knowledge and reduce search dimensionality. The uncertainty-led exploration/exploitation sampling strategy delivers global minima with modest sampling, but also ensures visits to less favorable regions of phase space to gather information on rare events and energy barriers.


